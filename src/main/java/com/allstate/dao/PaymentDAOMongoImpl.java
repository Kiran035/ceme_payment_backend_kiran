package com.allstate.dao;

import com.allstate.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PaymentDAOMongoImpl implements PaymentDAO
{

    @Autowired
    MongoTemplate tpl;

    @Override
    public int rowCount() {
        Query query = new Query();
        long count = tpl.count(query, Payment.class);
        return (int) count;

        /*
           return type kept as int to match the assignment requirement document

           To match the requirement document we are parsing the long value to int
           YIf you update return type to long then no parsing required because by default mongo count function returns
           value in long

         */
        //return count ;

    }

    @Override
    public Payment findById(int id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        return tpl.findOne(query,Payment.class);

    }

    @Override
    public List<Payment> findByType(String type) {
        Query query = new Query();
        query.addCriteria(Criteria.where("type").is(type));
        return tpl.find(query,Payment.class);
    }

    @Override
    public List<Payment> getAllPayments() {
        return tpl.findAll(Payment.class);
    }

    @Override
    public int save(Payment payment) {
        Payment a = tpl.save(payment);
        return a.getId();
    }

    @Override
    public void delete(Payment payment) {
        tpl.remove(payment);
    }
}
