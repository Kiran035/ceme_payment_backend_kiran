package com.allstate.dao;

import com.allstate.entities.Payment;

import java.util.List;

public interface PaymentDAO {

    int rowCount();
    Payment findById(int id);
    List<Payment> findByType(String type);
    List<Payment> getAllPayments();
    int save(Payment payment);
    void delete(Payment payment);

}
