package com.allstate.service;

import com.allstate.Exception.PaymentException;
import com.allstate.dao.PaymentDAO;
import com.allstate.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    PaymentDAO dao;

    @Override
    public int rowCount() {
        return dao.rowCount();
    }

    @Override
    public Payment findById(int id) {

        if(id>0)
            return dao.findById(id);
        else
            throw new PaymentException("Invalid Id, Id should be greater than 0");
    }

    @Override
    public List<Payment> findByType(String type) {

        if(type!=null && !type.isEmpty())
            return dao.findByType(type);
        else
            throw new PaymentException("Invalid type, Type should not be null or empty");

    }

    @Override
    public int save(Payment payment) {
        return dao.save(payment);
    }

    @Override
    public List<Payment> getAllPayments() {
        try{
            return dao.getAllPayments();
        }catch(Exception ex)
        {
            throw new PaymentException("Error while fetching payment details - " + ex.getMessage());

        }
    }

    @Override
    public void delete(Payment payment) {
        dao.delete(payment);
    }
}
