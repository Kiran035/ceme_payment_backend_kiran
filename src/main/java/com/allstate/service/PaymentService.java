package com.allstate.service;

import com.allstate.entities.Payment;

import java.util.List;

public interface PaymentService {
    int rowCount();
    Payment findById(int id);
    List<Payment> findByType(String type);
    int save(Payment payment);
    List<Payment> getAllPayments();
    void delete(Payment payment);
}
