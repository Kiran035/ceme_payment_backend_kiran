package com.allstate.rest;

import com.allstate.Exception.PaymentException;
import com.allstate.entities.Payment;
import com.allstate.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.lang.reflect.Method;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/payment")
public class PaymentControllerImpl implements PaymentController {

    @Autowired
    PaymentService service;

    @Override
    @RequestMapping(value="/status", method = RequestMethod.GET)
    public String status()
    {
        return "Payment API is running";
    }


    @Override
    @RequestMapping(value="/count", method = RequestMethod.GET)
    public int rowCount() {
        return service.rowCount();
    }

    @Override
    @RequestMapping(value="/findById/{id}", method = RequestMethod.GET)
    public ResponseEntity<Payment> findById(@PathVariable("id") int id) {
        try{
            Payment p =  service.findById(id);
            return new ResponseEntity<>(p, HttpStatus.OK);
        }catch (PaymentException e)
        {
            return new ResponseEntity(e.getMessage(),null,HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @RequestMapping(value="/findByType/{type}", method = RequestMethod.GET)
    public ResponseEntity<List<Payment>> findByType(@PathVariable("type") String type) {
            try{
                List<Payment> list =  service.findByType(type);
                return new ResponseEntity<>(list, HttpStatus.OK);
            }catch (PaymentException e)
            {
                return new ResponseEntity(e.getMessage(),null,HttpStatus.BAD_REQUEST);
                //return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
    }

    @Override
    @RequestMapping(value="/find", method = RequestMethod.GET)
    public ResponseEntity<List<Payment>> findByType2(@RequestParam("type") String type) {
        try{
            List<Payment> list =  service.findByType(type);
            return new ResponseEntity<>(list, HttpStatus.OK);
        }catch (PaymentException e)
        {
            return new ResponseEntity(e.getMessage(),null,HttpStatus.BAD_REQUEST);
            //return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<List<Payment>> getAllPayments() {
        try{
            List<Payment> list =  service.getAllPayments();
            return new ResponseEntity<>(list, HttpStatus.OK);
        }catch (PaymentException e)
        {
            return new ResponseEntity(e.getMessage(),null,HttpStatus.NOT_FOUND);
            //return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }


    @Override
    @RequestMapping(value="/save", method = RequestMethod.POST)
    public void save(@RequestBody Payment payment) {
        service.save(payment);
    }

    @Override
    @RequestMapping(value="/delete", method = RequestMethod.POST)
    public void delete(@RequestBody Payment payment) {
        service.delete(payment);
    }

}
