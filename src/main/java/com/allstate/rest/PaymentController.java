package com.allstate.rest;

import com.allstate.entities.Payment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface PaymentController {
    @RequestMapping(value="/status", method = RequestMethod.GET)
    String status();

    @RequestMapping(value="/count", method = RequestMethod.GET)
    int rowCount();

    @RequestMapping(value="/findById/{id}", method = RequestMethod.GET)
    ResponseEntity<Payment> findById(@PathVariable("id") int id);

    @RequestMapping(value="/findByType/{type}", method = RequestMethod.GET)
    ResponseEntity<List<Payment>> findByType(@PathVariable("type") String type);

    @RequestMapping(value="/find", method = RequestMethod.GET)
    ResponseEntity<List<Payment>> findByType2(@RequestParam("type") String type);

    @RequestMapping(value="/all", method = RequestMethod.GET)
    ResponseEntity<List<Payment>> getAllPayments();


    @RequestMapping(value="/save", method = RequestMethod.POST)
    void save(Payment payment);

    @RequestMapping(value="/delete", method = RequestMethod.POST)
    void delete(Payment payment);
}
