package com.allstate.Exception;

public class PaymentException extends  RuntimeException{

    public PaymentException(String msg)
    {
        super(msg);
    }
}
