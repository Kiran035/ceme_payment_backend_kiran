package com.allstate.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class Payment {
    @Id
    private int id;
    private Date paymentDate;
    private String type;
    private double amount;
    private int custID;

    public Payment() {

    }

    public Payment(int id, Date paymentDate, String type, double amount, int custID) {
        this.id = id;
        this.paymentDate = paymentDate;
        this.type = type;
        this.amount = amount;
        this.custID = custID;
    }

    public int getId() {
        return id;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public String getType() {
        return type;
    }

    public double getAmount() {
        return amount;
    }

    public int getCustID() {
        return custID;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void setCustID(int custID) {
        this.custID = custID;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "id=" + id +
                ", paymentDate=" + paymentDate +
                ", type='" + type + '\'' +
                ", amount=" + amount +
                ", custID=" + custID +
                '}';
    }
}
