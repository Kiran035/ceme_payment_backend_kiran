package com.allstate.entities;

import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.Date;
import java.util.Random;


import static org.junit.jupiter.api.Assertions.*;

class PaymentTest {

    private Payment payment;

    private int id;
    private Date date;
    private String type;
    private double amount;
    private int custID;


    @BeforeEach
    void setUp() {
        id = (new Random()).nextInt();
        date = new Date();
        type = "Saving";
        amount = (new Random()).nextDouble();
        custID = (new Random()).nextInt();
        payment = new Payment(id,date,type,amount,custID);

        System.out.println(date);
    }

    @AfterEach
    void tearDown() {
        payment = null;
    }

    @Test
    void getId() {
        assertEquals(id,payment.getId());
    }

    @Test
    void getPaymentDate() {
        assertEquals(date,payment.getPaymentDate());
    }

    @Test
    void getType() {
        assertEquals(type,payment.getType());
    }

    @Test
    void getAmount() {
        assertEquals(amount,payment.getAmount());
    }

    @Test
    void getCustID() {
        assertEquals(custID,payment.getCustID());
    }

    @Test
    void setId() {
        id = (new Random()).nextInt();
        payment.setId(id);
        assertEquals(id,payment.getId());
    }

    @Test
    void setPaymentDate() {
        date = new Date();
        payment.setPaymentDate(date);
        assertEquals(date,payment.getPaymentDate());
    }

    @Test
    void setType() {
        String type = "current";
        payment.setType(type);
        assertEquals(type,payment.getType());
    }

    @Test
    void setAmount() {
        amount = (new Random().nextDouble());
        payment.setAmount(amount);
        assertEquals(amount,payment.getAmount());
    }

    @Test
    void setCustID() {
        custID = (new Random().nextInt());
        payment.setCustID(custID);
        assertEquals(custID,payment.getCustID());
    }

    @Test
    void testToString() {
        String exp = "Payment{" +
                "id=" + id +
                ", paymentDate=" + date +
                ", type='" + type + '\'' +
                ", amount=" + amount +
                ", custID=" + custID +
                '}';
        assertEquals(exp, payment.toString());
    }
}