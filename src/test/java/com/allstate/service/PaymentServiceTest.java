package com.allstate.service;

import com.allstate.Exception.PaymentException;
import com.allstate.dao.PaymentDAO;
import com.allstate.entities.Payment;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.function.Executable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.Date;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PaymentServiceTest {

    @Autowired
    PaymentService service;

    @Autowired
    MongoTemplate tpl;

    private Payment payment;

    private int id;
    private Date date;
    private String type;
    private double amount;
    private int custID;



    @BeforeAll
    void setUp() {
        id = (new Random()).nextInt(Integer.MAX_VALUE);
        date = new Date();
        type = "Deposit";
        amount = (new Random()).nextInt(Integer.MAX_VALUE);
        custID = (new Random()).nextInt(Integer.MAX_VALUE);
        payment = new Payment(id,date,type,amount,custID);
        // Saving or Data Setup Just to help other test to use runtime data to test ById, ByType, Count etc...
        service.save(payment);
    }

    @AfterAll
    void tearDown() {
        tpl.remove(payment);
    }



    @Test
    void save() {
        service.save(payment);
        Payment temp = service.findById(payment.getId());
        assertEquals(payment.toString(),temp.toString());
    }

    @Test
    void rowCount() {
        Assertions.assertTrue(service.rowCount()>0, "No Records returned from DB");
    }

    @Test
    void findById() {
        Payment act = service.findById(id);
        assertEquals(id,act.getId());
    }

    @Test
    void findById_Negative() {
        Assertions.assertThrows(PaymentException.class, new Executable() {
            @Override
            public void execute() throws Throwable {
                Payment act = service.findById(0);
            }
        });

    }

    @Test
    void findByType() {
        List<Payment> actList = service.findByType(type);
        //Check value are returning
        assertTrue(actList.size()>0);

        //Check each object type matches the given type
        assertTrue(actList.stream().allMatch(a -> a.getType().equalsIgnoreCase(type)));
    }

    @Test
    void findByType_Negative() {


        Assertions.assertThrows(PaymentException.class, new Executable() {
            @Override
            public void execute() throws Throwable {
                List<Payment> actList = service.findByType(null);
            }
        });
    }

    @Test
    void getAllPayment() {
        List<Payment> actList = service.getAllPayments();
        //Check value are returning
        assertTrue(actList.size()>0);

    }

    @Test
    void delete() {
        Payment payment = new Payment(1,new Date(),"deposit",111,111);
        tpl.save(payment);
        int r1 = tpl.findAll(Payment.class).size();
        service.delete(payment);
        int r2 = tpl.findAll(Payment.class).size();
        assertEquals(r1-1,r2);

    }
}