package com.allstate.rest;

import com.allstate.entities.Payment;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Date;

@SpringBootTest
class PaymentControllerTest {


    private final RestTemplate restTemplate;


    @Autowired
    public PaymentControllerTest(RestTemplateBuilder builder) {
        this.restTemplate = builder.build();
    }

    @Test
    void status() {
        Assertions.assertEquals("Payment API is running",restTemplate.getForObject("http://localhost:8080/api/payment/status",String.class));

    }


    @Test
    void save_and_findById() {
        Payment payment = new Payment(2,new Date(),"deposit",1,1);
        restTemplate.postForEntity("http://localhost:8080/api/payment/save", payment, Payment.class);
        Payment returnObj = restTemplate.getForObject("http://localhost:8080/api/payment/findById/2",Payment.class);
        Assertions.assertEquals(payment.toString(),returnObj.toString());
    }

    @Test
    void getAllPayment() {
        ResponseEntity<Object[]> responseEntity = restTemplate.getForEntity("http://localhost:8080/api/payment/all", Object[].class);
        Object[] objects = responseEntity.getBody();
        HttpStatus statusCode = responseEntity.getStatusCode();
        Assertions.assertEquals(200,statusCode.value());
    }
}