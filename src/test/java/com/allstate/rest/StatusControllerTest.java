package com.allstate.rest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestTemplate;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
class StatusControllerTest {


    private final RestTemplate restTemplate;


    @Autowired
    public StatusControllerTest(RestTemplateBuilder builder) {
        this.restTemplate = builder.build();
    }

    @Test
    void getStatus() {
        Assertions.assertEquals("Rest API is running",restTemplate.getForObject("http://localhost:8080/api/status",String.class));

    }
}