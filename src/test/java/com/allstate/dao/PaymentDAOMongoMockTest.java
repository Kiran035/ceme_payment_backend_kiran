package com.allstate.dao;

import com.allstate.entities.Payment;
import org.junit.jupiter.api.*;
import static org.mockito.Mockito.*;

import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.Date;
import java.util.List;
import java.util.Random;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

//@SpringBootTest
class PaymentDAOMongoMockTest {

    @Mock
    PaymentDAO dao;

    @Mock
    private Payment payment;

    @Mock
    private List<Payment> list;


    @BeforeEach
    void setUp() {
        initMocks(this);
    }



    @Test
    void save() {
        doReturn(1).when(dao).save(payment);
        assertEquals(1, dao.save(payment));
    }

    @Test
    void rowCount() {
        doReturn(1).when(dao).rowCount();
        int temp = dao.rowCount();
        assertEquals(1,temp);
    }

    @Test
    void findById() {
        doReturn(payment).when(dao).findById(2);
        assertEquals(payment.getId(),dao.findById(2).getId());
    }

    @Test
    void findByType() {
        doReturn(list).when(dao).findByType("savings");
        assertEquals(list,dao.findByType("savings"));

    }


}