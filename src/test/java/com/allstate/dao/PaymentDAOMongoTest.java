package com.allstate.dao;

import com.allstate.entities.Payment;
import org.junit.jupiter.api.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PaymentDAOMongoTest {

    @Autowired
    PaymentDAO dao;

    @Autowired
    MongoTemplate tpl;

    private Payment payment;

    private int id;
    private Date date;
    private String type;
    private double amount;
    private int custID;



    @BeforeAll
    void setUp() throws ParseException {

        Random rnd = new Random();
        id = rnd.nextInt(Integer.MAX_VALUE);

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        date = new Date();
        String temp = formatter.format(date);
        date = formatter.parse(temp);


        type = "withdrawal";
        amount = rnd.nextInt(Integer.MAX_VALUE);
        custID = rnd.nextInt(Integer.MAX_VALUE);
        payment = new Payment(id,date,type,amount,custID);

        // Saving or Data Setup Just to help other test to use runtime data to test ById, ByType, Count etc...
        dao.save(payment);
    }

    @AfterAll
    void tearDown() {
       tpl.remove(payment);
    }

    @Test
    void save() {
        dao.save(payment);   //This line can be commented if required, because @BeforeEach last line is doing Required data setup
        Payment temp = dao.findById(payment.getId());
        assertEquals(payment.toString(),temp.toString());
    }

    @Test
    void rowCount() {
        Assertions.assertTrue(dao.rowCount()>0, "No Records returned from DB");
    }

    @Test
    void findById() {
        Payment act = dao.findById(id);
        assertEquals(id,act.getId());
    }

    @Test
    void findByType() {
        List<Payment> actList = dao.findByType(type);
        //Check value are returning
        assertTrue(actList.size()>0);

        //Check each object type matches the given type
        assertTrue(actList.stream().allMatch(a -> a.getType().equalsIgnoreCase(type)));
    }

    @Test
    void getAllPayment() {
        List<Payment> actList = dao.getAllPayments();
        //Check value are returning
        assertTrue(actList.size()>0);

    }

    @Test
    void delete() {
        Payment payment = new Payment(1,new Date(),"deposit",111,111);
        tpl.save(payment);
        int r1 = tpl.findAll(Payment.class).size();
        dao.delete(payment);
        int r2 = tpl.findAll(Payment.class).size();
        assertEquals(r1-1,r2);

    }


}